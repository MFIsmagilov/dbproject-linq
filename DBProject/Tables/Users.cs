﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;

namespace DBProject.Tables
{
    [Table(Name = "Users")]
    public class Users: IEssenceDB
    {
        public Users()
        {
            _idRoles = new EntityRef<Roles>();
        }

        public EntityRef<Roles> _idRoles;
        [Association(Storage = "_idRoles", ThisKey = "IdUser", OtherKey = "IdRole", Name = "FK_users_roles_", IsForeignKey = true)]
        public Roles IdRoles
        {
            get { return _idRoles.Entity; }
            set { _idRoles.Entity = value; }
        }

        private int _IdUser;
        [Column(Name = "IdUser", Storage = "_IdUser", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdUser
        {
            get { return _IdUser; }
            set { _IdUser = value; }
        }

        private string _Login;
        [Column(Name = "Login", Storage = "_Login", DbType = "VARCHAR(100)", CanBeNull = false)]
        public string Login
        {
            get { return _Login; }
            set { _Login = value; }
        }

        private string _Password;
        [Column(Name = "Password", Storage = "_Password", DbType = "VARCHAR(255)", CanBeNull = false)]
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        private string _Phone;
        [Column(Name = "Phone", Storage = "_Phone", DbType = "VARCHAR(100)", CanBeNull = false)]
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        private string _SurnameUser;
        [Column(Name = "SurnameUser", Storage = "_SurnameUser", DbType = "VARCHAR(100)", CanBeNull = false)]
        public string SurnameUser
        {
            get { return _SurnameUser; }
            set { _SurnameUser = value; }
        }

        public string _NameUser;
        [Column(Name = "NameUser", Storage = "_NameUser", DbType = "VARCHAR(100)", CanBeNull = false)]
        public string NameUser
        {
            get { return _NameUser; } 
            set { _NameUser = value; }
        }

        public int? _idRole;
        [Column(Name = "IdRole", Storage = "idRole", DbType = "INT(10)", CanBeNull = false)]
        public int? IdRole
        {
            get { return _idRole; }
            set { _idRole = value; }
        }

        public void SetValueAtribute(List<string> values)
        {
            this.IdUser = Convert.ToInt32(values[0]);
            this.SurnameUser = values[1];
            this.NameUser = values[2];
            this.Password = values[3];
            this.Login = values[4];
            this.Phone = values[5];
            this.IdRole = Convert.ToInt32(values[6]);
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdUser");
            l.Add("SurnameUser");
            l.Add("NameUser");
            l.Add("Password");
            l.Add("Login");
            l.Add("Phone");
            l.Add("IdRole");
            return l;
        }
    }
    //[Table(Name = "Users")]
    //public class Users
    //{
    //    [Column(Name = "IdUser", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
    //    public int IdUser { get; set; }
    //    [Column(Name = "Login", DbType = "VARCHAR(100)", CanBeNull = false)]
    //    public string Login { get; set; }
    //    [Column(Name = "Password", DbType = "VARCHAR(255)", CanBeNull = false)]
    //    public string Password { get; set; }
    //    [Column(Name = "Phone", DbType = "VARCHAR(100)", CanBeNull = false)]
    //    public string Phone { get; set; }
    //    [Column(Name = "SurnameUser", DbType = "VARCHAR(100)", CanBeNull = false)]
    //    public string SurnameUser { get; set; }
    //    [Column(Name = "NameUser", DbType = "VARCHAR(100)", CanBeNull = false)]
    //    public string NameUser { get; set; }
    //    [Column(Name = "idRole", DbType = "INT(10)",  CanBeNull = false)]
    //    public int IdRole { get; set; }

    //}

    public class TypeUsers : ITypeTables
    {
        public int IdUser { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string SurnameUser { get; set; }
        public string NameUser { get; set; }
        public int? IdRole { get; set; }

        public TypeUsers(Users user)
        {
            this.IdUser = user.IdUser;
            this.SurnameUser = user.SurnameUser;
            this.NameUser = user.NameUser;
            this.Password = user.Password;
            this.Login = user.Login;
            this.Phone = user.Phone;
            this.IdRole = user.IdRole;
        }
        public TypeUsers(string login, string password, int role)
        {
            this.IdRole = role;
            this.Login = login;
            this.Password = password; 
        }
        public TypeUsers(int idUser, string login, string password, string phone, string surname, string name, int idRole)
        {
            this.IdUser = idUser;
            this.SurnameUser = surname;
            this.NameUser = name;
            this.Password = password;
            this.Login = login;
            this.Phone = phone;
            this.IdRole = idRole;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdUser");
            l.Add("SurnameUser");
            l.Add("NameUser");
            l.Add("Password");
            l.Add("Login");
            l.Add("Phone");
            l.Add("IdRole");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdUser.ToString());
            l.Add(this.SurnameUser);
            l.Add(this.NameUser);
            l.Add(this.Password);
            l.Add(this.Login);
            l.Add(this.Phone);
            l.Add(this.IdRole.ToString());
            return l.ToArray();
        }
        public override string ToString()
        {
            return "IdUser  : " + IdUser.ToString() + Environment.NewLine +
                   "Login   : " + Login + Environment.NewLine +
                   "Password: " + Password + Environment.NewLine +
                   "Surname : " + SurnameUser + Environment.NewLine +
                   "Name    : " + NameUser + Environment.NewLine +
                   "Phone   : " + Phone + Environment.NewLine  +
                   "idRole  : " + IdRole.ToString() + Environment.NewLine;
        }
    }
}
