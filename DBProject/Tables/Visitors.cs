﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Посетители"
    /// </summary>
    [Table(Name = "Visitors")]
    public class Visitors:IEssenceDB
    {

        public Visitors()
        {
            _guest = new EntityRef<Guests>();
        }

        private int _Id;
        [Column(Name = "Id", Storage = "_Id",DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int Id 
        { 
            get {return _Id;}
            set { _Id = value;}
        }

        private bool _Advertising;
        [Column(Name = "Advertising", Storage = "_Advertising", DbType = "TINYINT(1)", CanBeNull = true)]
        public bool Advertising
        { 
            get { return _Advertising;}
            set { _Advertising = value; }
        }

        private bool _AdBlock;
        [Column(Name = "AdBlock", Storage = "_AdBlock", DbType = "TINYINT(1)", CanBeNull = true)]
        public bool AdBlock
        {
            get { return _AdBlock; }
            set { _AdBlock = value; }
        }

        private string _Country;
        [Column(Name = "Country", Storage = "_Country", DbType = "VARCHAR(55)", CanBeNull = true)]
        public string Country 
       {
           get { return _Country; }
            set {_Country =  value;}
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("Advertising");
            l.Add("AdBlock");
            l.Add("Country");
            return l;      
        }

        public void SetValueAtribute(List<string> values)
        {
            this.Id = Convert.ToInt32(values[0]);
            this.Advertising = Convert.ToBoolean(values[1]);
            this.AdBlock = Convert.ToBoolean(values[2]);
            this.Country = values[3];
        }

        public EntityRef<Guests> _guest;
        [Association(Storage = "_guest", ThisKey = "Id", OtherKey = "Id", Name = "FK_visitors_guest", IsForeignKey = true)]
        public Guests Idg
        {
            get { return _guest.Entity; }
            set { _guest.Entity = value; }
        }
    }

    public class TypeVisitor : ITypeTables
    {
        public int Id { get; set; }
        public bool Advertising { get; set; }
        public bool AdBlock { get; set; }
        public string Country { get; set; }

        public TypeVisitor(Visitors v)
        {
            this.Id = v.Id;
            this.Advertising = v.Advertising;
            this.AdBlock = v.AdBlock;
            this.Country = v.Country;
        }

        public TypeVisitor(int id, bool advertising, bool adBlock, string country)
        {
            this.Id = id;
            this.Advertising = advertising;
            this.AdBlock = AdBlock;
            this.Country = country;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("Advertising");
            l.Add("AdBlock");
            l.Add("Country");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.Id.ToString());
            l.Add(this.Advertising.ToString());
            l.Add(this.AdBlock.ToString());
            l.Add(this.Country);
            return l.ToArray();
        }

        public override string ToString()
        {
            return "Id          : " + Id.ToString() + "\n" +
                   "Advertising : " + Advertising.ToString() + "\n" +
                   "AdBlock     : " + AdBlock.ToString() + "\n" +
                   "Country     : " + Country + "\n";
        }

    }
}
