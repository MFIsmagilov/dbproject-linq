﻿using DBProject.Tables;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class Registration : Form
    {
        private GuestContext _context;
        private string connStr = String.Format("server={0};user={1}; password={2}; database={3}"
               , Connection.ServerName, Connection.UserName, Connection.Password, Connection.DbName);
        public Registration(GuestContext pCont)
        {
            _context = pCont;
            InitializeComponent();
            ToolTip tt = new ToolTip();
            //tt.IsBalloon = true;
            //tt.InitialDelay = 0;
            //tt.ShowAlways = true;
            //tt.SetToolTip(textBox2, "Максимальная длина - 8. Пароль должен содержать не менее 6 символов");
            //button1.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            loginTextBox.Text = "";
            passwordTextBox.Text = "";
            surnameTextBox.Text = "";
            nameTextBox.Text = "";
            phoneTextBox.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //3 empty role
            //TypeUsers user = new TypeUsers( 0, 
            //                                loginTextBox.Text, 
            //                                passwordTextBox.Text, 
            //                                phoneTextBox.Text, 
            //                                surnameTextBox.Text,
            //                                nameTextBox.Text, 
            //                                3);
            
            Users user = new Users{
                               
                                    Login = loginTextBox.Text,
                                    Password = passwordTextBox.Text,
                                    Phone = phoneTextBox.Text,
                                    SurnameUser = surnameTextBox.Text,
                                    NameUser = nameTextBox.Text,
                                    IdRole = 3};
            UserContext userContext = new UserContext(_context.Connection);
            
            //    userContext.Connection.Open();
       
            userContext.UsersTbl.InsertOnSubmit(user);
            userContext.SubmitChanges();
            userContext.Connection.Close();
            MessageBox.Show("Пользователь добавлен!");
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
