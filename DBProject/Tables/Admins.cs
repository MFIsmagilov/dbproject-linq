﻿using DbLinq.Data.Linq;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Администраторы"
    /// </summary>

    [Table(Name = "Admins")]
    public class Admins:IEssenceDB
    {
        public Admins()
        {
            _articles = new EntitySet<Articles>();
        }
        EntitySet<Articles> _articles;
        [Association(Storage = "_articles", Name = "FK_admins_articles", ThisKey = "IdAdmin", OtherKey = "IdArticle")]
        public EntitySet<Articles> Article
        {
            get { return _articles; }
            set { _articles = value; }
        }
        private int _IdAdmin;
        [Column(Name = "IdAdmin", Storage = "_IdAdmin", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdAdmin 
        {
            get { return _IdAdmin; }
            set { _IdAdmin = value; }
        }
        private string _Surname;
        [Column(Name = "Surname", Storage = "_Surname", DbType = "VARCHAR(255)", CanBeNull = false)]
        public string Surname 
         {
            get { return _Surname; }
             set { _Surname = value; }
        }
        private string _Name;
        [Column(Name = "Name", Storage = "_Name", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Name 
         {
            get { return _Name; }
             set { _Name = value; }
        }

        private string _Password;
        [Column(Name = "Password", Storage = "_Password", DbType = "VARCHAR(255)", CanBeNull = false)]
        public string Password 
          {
            get { return _Password; }
              set { _Password = value; }
        }

        public override string ToString()
        {
            return IdAdmin.ToString();
        }

        public void SetValueAtribute(List<string> values)
        {
            this.IdAdmin = Convert.ToInt32(values[0]);
            this.Surname = values[1];
            this.Name = values[2];
            this.Password = values[3];
        }
        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdAdmin");
            l.Add("Surname");
            l.Add("Name");
            l.Add("Password");
            return l;
        }
    }

    public class TypeAdmin : ITypeTables
    {
        public int IdAdmin { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }


        public TypeAdmin(Admins p)
        {
            this.IdAdmin = p.IdAdmin;
            this.Surname = p.Surname;
            this.Name = p.Name;
            this.Password = p.Password;
        }

        public TypeAdmin(int idAdmin, string surname, string name, string password)
        {
            this.IdAdmin = idAdmin;
            this.Surname = surname;
            this.Name = name;
            this.Password = password;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdAdmin");
            l.Add("Surname");
            l.Add("Name");
            l.Add("Password");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdAdmin.ToString());
            l.Add(this.Surname);
            l.Add(this.Name);
            l.Add(this.Password);
            return l.ToArray();
        }

        public override string ToString()
        {
            return "IdAdmin : " + IdAdmin.ToString() + "\n" +
                   "Surname : " + Surname + "\n" +
                   "Name    : " + Name + "\n" +
                   "Password: " + Password + "\n";
        }
        //public class TypeAdmins
        //{
        //    public int IdAdmin { get; set; }
        //    public string Surname { get; set; }
        //    public string Name { get; set; }
        //    public string Password { get; set; }

        //    public TypeAdmins(int idAdmin, string surname, string name, string password)
        //    {
        //        this.IdAdmin = idAdmin;
        //        this.Surname = surname;
        //        this.Name = name;
        //        this.Password = password;
        //    }

        //    public override string ToString()
        //    {
        //        return "IdAdmin : " + IdAdmin.ToString() + "\n" +
        //               "Surname : " + Surname + "\n" +
        //               "Name    : " + Name + "\n" +
        //               "Password: " + Password + "\n";
        //    }
        //}
    }
}
