﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Гости"
    /// </summary>
    [Table(Name = "Guests")]
    public class Guests:IEssenceDB
    {
        public Guests()
        {
            _idVis = new EntityRef<Visitors>();
        }
        public EntityRef<Visitors> _idVis;
        [Association(Storage = "_idVis", ThisKey = "Id", OtherKey = "Id", Name = "FK_guests_visitors_Id")]

        private string _Ip;
        [Column(Name = "Ip", Storage = "_Ip", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Ip 
        {
            get { return _Ip;}
            set { _Ip = value; } 
        }

        private string _City;
        [Column(Name = "City", Storage = "_City", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string City 
        {
            get { return _City;}
            set { _City = value; } 
        }

        private string _Provider;
        [Column(Name = "Provider", Storage = "_Provider", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Provider 
        {
            get { return _Provider;}
            set { _Provider  = value; } 
        }

        private int _Id;
        [Column(Name = "Id", Storage = "_Id", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int Id 
        {
            get { return _Id;}
            set { _Id = value; } 
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("Ip");
            l.Add("City");
            l.Add("Provider");
            return l;
        }

        public void SetValueAtribute(List<string> values)
        {
            this.Id = Convert.ToInt32(values[0]);
            this.Ip = values[1];
            this.City = values[2];
            this.Provider = values[3];
        }
    }
    public class TypeGuests : ITypeTables
    {
        public string Ip { get; set; }
        public string City { get; set; }
        public string Provider { get; set; }
        public int Id { get; set; }

        public TypeGuests(Guests g)
        {
            this.Ip = g.Ip;
            this.City = g.City;
            this.Provider = g.Provider;
            this.Id = g.Id;
        }

        public TypeGuests(int id, string ip, string city, string provider)
        {
            this.Id = id;
            this.Ip = ip;
            this.City = city;
            this.Provider = provider;
        }
        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("Ip");
            l.Add("City");
            l.Add("Provider");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.Id.ToString());
            l.Add(this.Ip);
            l.Add(this.City);
            l.Add(this.Provider);
            return l.ToArray();
        }
        public override string ToString()
        {
            return "Id      : " + Id.ToString() + "\n" +
                   "Ip      : " + Ip + "\n" +
                   "City    : " + City + "\n" +
                   "Provider: " + Provider + "\n";
        }
    }
}
