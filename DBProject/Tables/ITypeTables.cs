﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBProject.Tables
{
    interface IEssenceDB
    {
        List<string> GetNameAtribute();
        void SetValueAtribute(List<string> values);
    }

    interface ITypeTables
    {
        //Возравщает список имен колонок в табл
        List<string> GetNameAtribute();

        //возваращет данные в текущей таблице
        object[] GetObject();

        //устаовка данных

    }
}
