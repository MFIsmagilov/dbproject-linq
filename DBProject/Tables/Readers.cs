﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Читают"(М:М)
    /// </summary>
    /// 

    [Table(Name = "Readers")]
    public class Readers:IEssenceDB
    {
        private int _Id;
        [Column(Name = "Id",Storage ="_Id", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int Id 
        {
            get { return _Id; }
            set { _Id = value; } 
        }

        private int _IdArticle;
        [Column(Name = "IdArticle", Storage = "_IdArticle", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdArticle 
        {
            get { return _IdArticle ;}
            set { _IdArticle = value; }
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("IdArticle");
            return l;    
        }

        public void SetValueAtribute(List<string> values)
        {
            this.Id = Convert.ToInt32(values[0]);
            this.IdArticle = Convert.ToInt32(values[1]);
        }
    }
    //[Table(Name = "Reads")]
    //public class Reads
    //{
    //    [Column(Name = "Id", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
    //    public int Id { get; set; }
    //    [Column(Name = "IdArticle", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
    //    public int IdArticle { get; set; }
    //}
    public class TypeReader : ITypeTables
    {
        public int Id { get; set; }
        public int IdArticle { get; set; }

        public TypeReader(Readers r)
        {
            this.Id = r.Id;
            this.IdArticle = r.IdArticle;
        }
        public TypeReader(int id, int idArticle)
        {
            this.Id = id;
            this.IdArticle = idArticle;
        }
        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("Id");
            l.Add("IdArticle");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.Id.ToString());
            l.Add(this.IdArticle.ToString());
            return l.ToArray();
        }
        public override string ToString()
        {
            return "Id        : " + Id.ToString() + "\n" +
                   "IdArticle : " + IdArticle.ToString() + "\n";
        }
    }
}
