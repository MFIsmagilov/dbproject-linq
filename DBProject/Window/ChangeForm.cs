﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class ChangeForm : Form
    {
        private Label[] labels;
        private TextBox[] textBoxes;
        private int countElement;
        string[] namesHeaders;
        string [] values;
        string nameTable;
        private AdminContext _context;

        public ChangeForm(AdminContext context, string[] namesHeaders, string[] values, string nameTable)
        {
            InitializeComponent();
            this._context = context;
            this.nameTable = nameTable;
            this.namesHeaders = namesHeaders;
            this.values = values;
            this.countElement = namesHeaders.Count();
            labels = new Label[countElement];
            textBoxes = new TextBox[countElement];

        }
        private void ChangeForm_Load(object sender, EventArgs e)
        {
            int height = 0;
            for (int i = 0; i < countElement; i++)
            {
                height = 60 + i * 60;
                labels[i] = new Label();
                labels[i].Text = namesHeaders[i];
                labels[i].Name = namesHeaders[i];
                labels[i].Location = new Point(20, height);
                labels[i].Size = new System.Drawing.Size(60, 20);
                labels[i].Visible = true;
                this.Controls.Add(labels[i]);

                textBoxes[i] = new TextBox();
                textBoxes[i].Location = new Point(120, height);
                textBoxes[i].Text = values[i];
                textBoxes[i].Name = values[i];
                textBoxes[i].Visible = true;
                this.Controls.Add(textBoxes[i]);
            }
            this.button1.Location = new Point(button1.Location.X, height += 30);
            this.button2.Location = new Point(button2.Location.X, height);
            this.Size = new Size(this.Size.Width, height + 70);
            this.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void очиститьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < countElement; i++)
                textBoxes[i].Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> values = new List<string>();
                for (int i = 0; i < textBoxes.Length; i++)
                    values.Add(textBoxes[i].Text);
                _context.UpdateObject(this.nameTable, values);

                this.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString(), "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
