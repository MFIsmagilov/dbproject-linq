﻿using DBProject.Tables;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using static DBProject.Program;

namespace DBProject
{

    public partial class AdminRole : Form
    {
        MySqlConnection con;
        MySqlCommand command;
        MySqlDataAdapter adapter;
        private AdminContext _context;
        private EmptyForm _ParentForm;
        public AdminRole(EmptyForm ParentForm, AdminContext context)
        {
            _ParentForm = ParentForm;
            _context = context;
            _context.Connection.Open();
            InitializeComponent();
            //ToolTip tt = new ToolTip();
        }

        //добавляются имена таблиц в дерево
        private void LoadAllFormsInTree()
        {
        }

        private void LoadAllFormsInTreeLINQ()
        {
            //GuestContext context = new GuestContext(new MySqlConnection(Connection.getConnectionString())); //new GuestContext(_context.Connection);
            //context.Connection.Open();
            TreeNode nodeTable = new TreeNode("Таблицы");
            treeNameTables.Nodes.Add(nodeTable);
            var linqSQL = (from p in _context.Mapping.GetTables() select p).ToList();
            foreach (var table in linqSQL)
            {
                TreeNode node = new TreeNode(table.TableName);
                treeNameTables.Nodes[0].Nodes.Add(node);
            }
        }
        private void AdminRole_Load(object sender, EventArgs e)
        {
            UserContext users = new UserContext(_context.Connection);

            var linqSql = (from p in users.UsersTbl
                           where p.IdRole == 3
                           select new TypeUsers(p)).ToList();

            foreach (var l in linqSql)
            {
                textBox1.Text += l.ToString();
                textBox1.Text += "_______________________";
            }
            LoadAllFormsInTreeLINQ();
        }
        private void AdminRole_FormClosed(object sender, FormClosedEventArgs e)
        {
            _ParentForm.Visible = true;
        }

        private void fillDataGrid<T>(List<T> linqSql)
        {
            if (linqSql != null)
            {
                MethodInfo GetNameAtribute = linqSql[0].GetType().GetMethod("GetNameAtribute");
                List<string> listAtribute = (List<string>)GetNameAtribute.Invoke(linqSql[0], null);
                foreach (var data in listAtribute)
                {
                    dgvDataTable.Columns.Add(data.ToString(), data.ToString());
                }
                foreach (var row in linqSql)
                {
                    MethodInfo GetMethod = row.GetType().GetMethod("GetObject");
                    dgvDataTable.Rows.Add((object[])GetMethod.Invoke(row, null));
                }
            }
            /*var listAtribute = linqSql[0].GetNameAtribute();
            foreach (var data in listAtribute)
            {
                dgvDataTable.Columns.Add(data, data);
            }
            foreach (var row in linqSql)
            {
                dgvDataTable.Rows.Add(row.GetObject());
            }*/
        }
        //заполняем dataGridView

        private void fillDataGrid(string nameTable)
        {
            var property = _context.GetType().GetProperty(nameTable);
            var enumerator = property.GetType().GetMethod("GetEnumerator");
            var e = enumerator.Invoke(property, null);


        }
        private void treeNameTables_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            dgvDataTable.Columns.Clear();
            //fillDataGrid(treeNameTables.SelectedNode.Text);
            UpdateTable(treeNameTables.SelectedNode.Text);
        }
        public void UpdateTable(string nameTable)
        {
            dgvDataTable.Columns.Clear();
            switch (nameTable)
            {
                case "Admins":
                    var l = (from p in _context.Admins select new TypeAdmin(p)).ToList();
                    fillDataGrid<TypeAdmin>(l);
                    break;
                case "Accounts":
                    var l1 = (from p in _context.Accouts select new TypeAccount(p)).ToList();
                    fillDataGrid<TypeAccount>(l1);
                    break;
                case "Readers":
                    var l2 = (from p in _context.Reads select new TypeReader(p)).ToList();
                    fillDataGrid<TypeReader>(l2);
                    break;
                case "Visitors":
                    var l3 = (from p in _context.Visitors select new TypeVisitor(p)).ToList();
                    fillDataGrid<TypeVisitor>(l3);
                    break;
                case "Regular_visitors":
                    var l4 = (from p in _context.RegularVisitors select new TypeRegularVisitor(p)).ToList();
                    fillDataGrid<TypeRegularVisitor>(l4);
                    break;
                case "Comments":
                    var l5 = (from p in _context.Comments select new TypeComment(p)).ToList();
                    fillDataGrid<TypeComment>(l5);
                    break;
                case "Guests":
                    var l6 = (from p in _context.Guests select new TypeGuests(p)).ToList();
                    fillDataGrid<TypeGuests>(l6);
                    break;
                case "Articles":
                    var l7 = (from p in _context.Articles select new TypeArticle(p)).ToList();
                    fillDataGrid<TypeArticle>(l7);
                    break;
                case "Roles":
                    var l8 = (from p in _context.Roles select new TypeRoles(p)).ToList();
                    fillDataGrid<TypeRoles>(l8);
                    break;
                case "Users":
                    var l9 = (from p in _context.Users select new TypeUsers(p)).ToList();
                    fillDataGrid<TypeUsers>(l9);
                    break;

            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            string[] namesHeaders = new string[dgvDataTable.ColumnCount];

            for (int i = 0; i < dgvDataTable.ColumnCount; i++)
            {
                namesHeaders[i] = dgvDataTable.Columns[i].HeaderText;
            }
            string nameTable = treeNameTables.SelectedNode.Text;
            InsertForm insertForm = new InsertForm(this._context, namesHeaders, nameTable);
            //insertForm.Visible = true;
            insertForm.Owner = this;
            insertForm.ShowDialog();

            #region Old add
            //if (dgvDataTable.SelectedRows.Count != 1)
            //{
            //    MessageBox.Show("Выделите одну строчку, которую хотите добавить",
            //                    "Внимание!",
            //                    MessageBoxButtons.OK,
            //                    MessageBoxIcon.Error);
            //}
            //else
            //{
            //    //    string query = "INSERT INTO " + treeNameTables.SelectedNode.Text + " (";
            //    string[] namesHeaders = new string[dgvDataTable.ColumnCount];

            //    for (int i = 0; i < dgvDataTable.ColumnCount; i++)
            //    {
            //        namesHeaders[i] = dgvDataTable.Columns[i].HeaderText;
            //    }

            //    //    query = query.Remove(query.Length - 2);
            //    //    query += " )";
            //    InsertForm insertForm = new InsertForm(namesHeaders);
            //    insertForm.Visible = true;
            //}
            #endregion
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (dgvDataTable.SelectedRows.Count != 1)
            {
                MessageBox.Show("Выделите одну строчку, которую хотите изменить",
                                "Внимание!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                string[] namesHeaders = new string[dgvDataTable.ColumnCount];
                string[] values = new string[dgvDataTable.ColumnCount];

                for (int i = 0; i < dgvDataTable.ColumnCount; i++)
                {
                    namesHeaders[i] = dgvDataTable.Columns[i].HeaderText;
                    values[i] = dgvDataTable[i, dgvDataTable.SelectedRows[0].Index].Value.ToString();
                }
                ChangeForm changeFrom = new ChangeForm(_context, namesHeaders, values, treeNameTables.SelectedNode.Text);
                changeFrom.Visible = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvDataTable.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выделите одну строчку, которую хотите удалить",
                                "Внимание!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                foreach (DataGridViewRow row in dgvDataTable.SelectedRows)
                {
                    List<string> listValues = new List<string>();
                    int indexRow = dgvDataTable.SelectedCells[0].RowIndex;

                    for (int i = 0; i < dgvDataTable.ColumnCount; i++)
                    {
                        listValues.Add(dgvDataTable[i, indexRow].Value.ToString());
                    }
                    _context.DeleteObject(treeNameTables.SelectedNode.Text, listValues);
                    dgvDataTable.Rows.Remove(row);
                }
            }
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateTable(treeNameTables.SelectedNode.Text);
        }



    }
}
