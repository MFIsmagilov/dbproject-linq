﻿using System.Text;
using System.Data;
using System.Data.Linq.Mapping;
using System;
using System.Diagnostics;
using System.Reflection;
using DbLinq.Data.Linq;
using DbLinq.Vendor;

using System.Collections.Generic;
using System.Linq;
using DbLinq.Vendor.Implementation;
using System.Configuration;
using MySql.Data.MySqlClient;
using DBProject.Tables;

namespace DBProject
{

    public class GuestContext : DataContext 
    {
        public GuestContext(IDbConnection iConn):base(iConn, new DbLinq.MySql.MySqlVendor())
        {
        }
        //public Table<Users> Users { get { return GetTable<Users>(); } }

    }
    public class AdminContext: GuestContext
    {
        public AdminContext(IDbConnection iConn)
            : base(iConn)
        {
        }

        private void GeneralFunctionForProcessingMethod(string nameTable, List<string> objects, string submit)
        {
            //получаем тип нашей таблицы nameTable
            Type type = Type.GetType("DBProject.Tables." + nameTable);

            //выхываем конструктор нашеего типа // Admins type = new Admin(); 
            object ObjTypeNameTable = Activator.CreateInstance(type);

            //у нашего объекта ищем метод SetValueAtribute
            var SetValueAtribute = ObjTypeNameTable.GetType().GetMethod("SetValueAtribute");

            //выполняем наш метод
            SetValueAtribute.Invoke(ObjTypeNameTable, new[] { objects });
            
            //получаем сво-ва this c именем нашей таблицы
            var propertyNameTable = this.GetType().GetProperty(nameTable).GetValue(this);
            //у этого сво-ва получаем метод InsertOnSubmit/DeleteOnsubmit
            var onSubmit = propertyNameTable.GetType().GetMethod(submit);
            //выполняем метод отправляя в него наше новый объект
            onSubmit.Invoke(propertyNameTable, new[] { ObjTypeNameTable });

            //аля сохраниться
            this.SubmitChanges();
        }

        public void AddObject(string nameTable, List<string> addingObject)
        {
            GeneralFunctionForProcessingMethod(nameTable, addingObject, "InsertOnSubmit");
        }

        public void DeleteObject(string nameTable, List<string> deletingObject)
        {
            GeneralFunctionForProcessingMethod(nameTable, deletingObject, "DeleteOnSubmit");
        }

        ///пока не используется
        private void helperObject(string nameTable, List<string> updatingObject)
        {
            Type type = Type.GetType(nameTable);
            var SetValueAtribute = type.GetMethod("SetValueAtribute");
            SetValueAtribute.Invoke(type, updatingObject.ToArray());
        }
        public void UpdateObject(string nameTable, List<string> updatingObject)
        { 
            switch(nameTable)
            {
                case "Admins":
                    Admins a = new Tables.Admins();
                    a.SetValueAtribute(updatingObject);
                    var l = (from p in this.Admins
                             where p.IdAdmin == a.IdAdmin
                             select p).Single<Admins>();
                    l.SetValueAtribute(updatingObject);
                    break;
                case "Users":
                    Users u = new Tables.Users();
                    u.SetValueAtribute(updatingObject);
                    var l1 = (from p in this.Users
                             where p.IdUser == u.IdUser
                             select p).Single<Users>();
                    l1.SetValueAtribute(updatingObject);
                    break;
                case "Roles":
                    Roles r = new Tables.Roles();
                    r.SetValueAtribute(updatingObject);
                    var l2 = (from p in this.Roles
                              where p.IdRole == r.IdRole
                              select p).Single<Roles>();
                    l2.SetValueAtribute(updatingObject);
                    break;
                case "Visitors":
                    Visitors v = new Tables.Visitors();
                    v.SetValueAtribute(updatingObject);
                    var l3 = (from p in this.Visitors
                              where p.Id == v.Id
                              select p).Single<Visitors>();
                    l3.SetValueAtribute(updatingObject);
                    break;
                
            }
            this.SubmitChanges();
        }
        public Table<Admins> Admins { get { return GetTable<Admins>(); } }
        public Table<Accounts> Accouts { get { return GetTable<Accounts>(); } }
        public Table<Readers> Reads { get { return GetTable<Readers>(); } }
        public Table<Visitors> Visitors { get { return GetTable<Visitors>(); } }
        public Table<Regular_visitors> RegularVisitors { get { return GetTable<Regular_visitors>(); } }
        public Table<Guests> Guests { get { return GetTable<Guests>(); } }
        public Table<Comments> Comments { get { return GetTable<Comments>(); } }
        public Table<Articles> Articles { get { return GetTable<Articles>(); } }
        public Table<Roles> Roles{ get { return GetTable<Roles>(); } }
        public Table<Users> Users { get { return GetTable<Users>(); } }
    }

    public class UserContext : GuestContext
    {
        public UserContext(IDbConnection iConn)
            : base(iConn)
        {
        }
        public Table<Users> UsersTbl { get { return GetTable<Users>(); } }

    }

    public class RolesContext : GuestContext
    {
        public RolesContext(IDbConnection iConn)
            : base(iConn)
        {
        }
        public Table<Roles> RolesTbl { get { return GetTable<Roles>(); } }

    }
   
    public class EmptyUserContext : GuestContext
    { public EmptyUserContext(IDbConnection iConn)
            : base(iConn)
        {
        }
        }
    public class CustomUserContext : EmptyUserContext
    {  
        public CustomUserContext(IDbConnection iConn)
            : base(iConn)
        {
        }
    }
    public class GuestUserContext : GuestContext
    {
        public GuestUserContext(IDbConnection iConn)
            : base(iConn)
        {
        }
    }

}
