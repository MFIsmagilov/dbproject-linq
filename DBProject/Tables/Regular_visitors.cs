﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Постоянные посетители"
    /// </summary>
    [Table(Name = "Regular_visitors")]
    public class Regular_visitors: IEssenceDB
    {
        private int _IdUsers;
        [Column(Name = "IdUsers", Storage = "_IdUsers", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdUsers 
        {
            get { return _IdUsers; }
            set { _IdUsers = value; } 
        }

        private string _Name;
        [Column(Name = "Name", Storage = "_Name", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _Surname;
        [Column(Name = "Surname", Storage = "_Surname", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }
        private string _Sex;
        [Column(Name = "Sex", Storage = "_Sex", DbType = "VARCHAR(255)", CanBeNull = true)]
        public string Sex
        {
            get { return _Sex; }
            set { _Sex = value; }
        }

        private int _Id;
        [Column(Name = "Id", Storage = "_Id", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdUsers");
            l.Add("Surname");
            l.Add("Name");
            l.Add("Sex");
            l.Add("Id");
            return l;   
        }

        public void SetValueAtribute(List<string> values)
        {
            this.IdUsers = Convert.ToInt32(values[0]);
            this.Surname = values[1];
            this.Name = values[2];
            this.Sex = values[3];
            this.Id = Convert.ToInt32(values[4]); 
        }
    }
    public class TypeRegularVisitor : ITypeTables
    {
        public int IdUsers { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Sex { get; set; }
        public int Id { get; set; }

        public TypeRegularVisitor(Regular_visitors rv)
        {
            this.IdUsers = rv.IdUsers;
            this.Name = rv.Name;
            this.Surname = rv.Surname;
            this.Sex = rv.Sex;
            this.Id = rv.Id;
        }
        public TypeRegularVisitor(int idUsers, string name, string surname, string sex, int id)
        {
            this.IdUsers = idUsers;
            this.Surname = surname;
            this.Name = name;
            this.Sex = sex;
            this.Id = id;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdUsers");
            l.Add("Surname");
            l.Add("Name");
            l.Add("Sex");
            l.Add("Id");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdUsers.ToString());
            l.Add(this.Surname);
            l.Add(this.Name);
            l.Add(this.Sex);
            l.Add(this.Id.ToString());
            return l.ToArray();
        }

        public override string ToString()
        {
            return "IdUsers  : " + IdUsers.ToString() + "\n" +
                   "Surname : " + Surname + "\n" +
                   "Name    : " + Name + "\n" +
                   "Sex     : " + Sex + "\n" +
                   "Id      : " + Id.ToString() + "\n";
        }
    }
}
