﻿using DBProject.Tables;
using MySql.Data.MySqlClient;
using System;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Dynamic;
using System.Windows.Forms;

namespace DBProject
{
    public partial class EmptyForm : Form
    {
        //private GuestContext _context;
        private GuestContext _context;
        private string connStr = String.Format("server={0};user={1}; password={2}; database={3}"
               , Connection.ServerName, Connection.UserName, Connection.Password, Connection.DbName);

        public EmptyForm()
        {
            InitializeComponent();
            loginTextBox.Text = "Логин";
            passwordTextBox.Text = "Пароль";

            _context = new GuestContext(new MySqlConnection(connStr));
            _context.Connection.Open();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /* Connection con = new Connection();
             //получает все данные из таблицы
             var linqSql = (from p in con.dataBase.AdminsTbl
                            select new TypeAdmin(
                                                  p.IdAdmin,
                                                  p.Surname,
                                                  p.Name,
                                                  p.Password));
             foreach (var l in linqSql) 
             textBox1.Text += l;*/
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void регистрацияToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Registration newForm = new Registration(_context);
            //newForm.ShowDialog(this);
            if (newForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (_context != null)
                {
                    _context.Connection.Close();
                    _context = new AdminContext(new MySqlConnection(connStr));
                    _context.Connection.Open();
                }

                MessageBox.Show("OK");
            }
            else
                MessageBox.Show("CANCEL");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //_context.Connection.Close();
            ///создаем и открываем новое подключение
            UserContext context = new UserContext(new MySqlConnection(connStr));
            context.Connection.Open();

            //Получаем все записи из таблиц users
      
            var linqSql = (from p in context.UsersTbl select new TypeUsers(p));
            context.Connection.Close();
           
            foreach (var p in linqSql)
                if (p.Login == loginTextBox.Text && p.Password == passwordTextBox.Text)
                {
                    RolesContext context1 = new RolesContext(new MySqlConnection(connStr));
                    context1.Connection.Open();
                    var roles = (from r in context1.RolesTbl//con.dataBase.UsersTbl
                                   //where (p.Login == loginTextBox.Text && p.Password == passwordTextBox.Text)
                                   select new TypeRoles(
                                       r.IdRole,
                                       r.NameRole
                                       ));
                    foreach (var role in roles)
                    {
                        if (p.IdRole == role.IdRole)
                        {

                            switch (role.NameRole)
                            {
                                case "admin"   :   AdminContext context2 = new AdminContext(new MySqlConnection(connStr));
                                                   AdminRole newForm = new AdminRole(this, context2);
                                                   newForm.Visible = true;
                                                   this.Visible = false;
                                                   break;
                                case "customer":   CustomUserContext context3 = new CustomUserContext(new MySqlConnection(connStr));
                                                   context3.Connection.Open();
                                                   Customer newForm1 = new Customer();
                                                   newForm1.Visible = true;
                                                   break;

                                case "empty": EmptyUserContext context4 = new EmptyUserContext(new MySqlConnection(connStr));
                                                   context4.Connection.Open();
                                                   EmptyRole newForm2 = new EmptyRole();
                                                   newForm2.Visible = true;
                                                   break;
                                case "guest": GuestUserContext context5 = new GuestUserContext(new MySqlConnection(connStr));
                                                   context5.Connection.Open();
                                                   GuestRole newForm3 = new GuestRole();
                                                   newForm3.Visible = true;
                                                   break;
                                                   
                            }

                            
                        }

                        
                    }
                    context1.Connection.Close();
                    return;
                }

            MessageBox.Show("Не найдено");
        }

        private void loginTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
