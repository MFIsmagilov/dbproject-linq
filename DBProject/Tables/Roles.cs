﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;

namespace DBProject.Tables
{
    //[Table(Name = "Roles")]
    //public class Roles
    //{
    //    [Column(Name = "IdRole", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
    //    public int IdRole { get; set; }
    //    [Column(Name = "NameRole", DbType = "VARCHAR(100)", CanBeNull = false)]
    //    public string NameRole { get; set; }
    //}

    [Table(Name = "Roles")]
    public class Roles:IEssenceDB
    {
        public Roles()
        {
            _user = new EntitySet<Users>();
        }
        private EntitySet<Users> _user;
        [Association(Storage = "_user", Name = "FK_users_roles_", ThisKey = "IdRole", OtherKey = "IdUser")]
        public EntitySet<Users> User
        {
            get { return _user; }
            set { _user = value; }
        }
        private int _IdRole;
        [Column(Name = "IdRole", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdRole
        {
            get { return _IdRole; }
            set { _IdRole = value; }
        }

        private string _NameRole;
        [Column(Name = "NameRole", DbType = "VARCHAR(100)", CanBeNull = false)]
        public string NameRole
        {
            get { return _NameRole; }
            set { _NameRole = value; }
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdRole");
            l.Add("NameRole");
            return l;
            
        }

        public void SetValueAtribute(List<string> values)
        {
            this.IdRole = Convert.ToInt32(values[0]);
            this.NameRole = values[1];
        }
    }

    public class TypeRoles : ITypeTables
    {
        public int IdRole { get; set; }
        public string NameRole { get; set; }

        public TypeRoles(Roles r)
        {
            this.IdRole = r.IdRole;
            this.NameRole = r.NameRole;
        }
        public TypeRoles(int idRole, string nameRole)
        {
            this.IdRole = idRole;
            this.NameRole = nameRole;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdRole");
            l.Add("NameRole");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdRole.ToString());
            l.Add(this.NameRole.ToString());
            return l.ToArray();
        }


        public override string ToString()
        {
            return "IdRole : " + IdRole.ToString() + "\n" +
                   "NameRole : " + NameRole + "\n";
        }
    }
}
