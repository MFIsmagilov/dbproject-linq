﻿using System.Data;
using DbLinq.Data.Linq;
using DbLinq.Vendor;
using DBProject.Tables;

namespace DBProject
{
    public partial class MySite : DataContext
    {

        public MySite(IDbConnection connection)
            : base(connection, new DbLinq.MySql.MySqlVendor())
        {
        }
        public MySite(IDbConnection connection, IVendor vendor)
            : base(connection, vendor)
        {
        }

        public Table<Admins> AdminsTbl { get { return GetTable<Admins>(); } }
        public Table<Accounts> AccoutsTbl { get { return GetTable<Accounts>(); } }
        public Table<Readers> ReadsTbl { get { return GetTable<Readers>(); } }
        public Table<Visitors> VisitorsTbl { get { return GetTable<Visitors>(); } }
        public Table<Regular_visitors> RegularVisitorsTbl { get { return GetTable<Regular_visitors>(); } }
        public Table<Guests> GuestsTbl { get { return GetTable<Guests>(); } }
        public Table<Comments> CommentsTbl { get { return GetTable<Comments>(); } }
        public Table<Articles> ArticlesTbl { get { return GetTable<Articles>(); } }
        public Table<Roles> RolesTbl { get { return GetTable<Roles>(); } }
    }
}