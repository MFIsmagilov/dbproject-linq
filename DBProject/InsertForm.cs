﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class InsertForm : Form
    {
        private Label[] labels;
        private TextBox[] textBoxes;
        private int countElement;
        string[] namesHeaders;
        string query;
        public InsertForm(string[] namesHeaders)
        {
            InitializeComponent();
            this.namesHeaders = namesHeaders;
            this.countElement = namesHeaders.Count();
            labels = new Label[countElement];
            textBoxes = new TextBox[countElement];
        }
        public InsertForm(string[] namesHeaders, string query)
        {
            InitializeComponent();
            this.query = query;
            this.namesHeaders = namesHeaders;
            this.countElement = namesHeaders.Count();
            labels = new Label[countElement];
            textBoxes = new TextBox[countElement];
        }

        private void InsertForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < countElement; i++)
            {
                labels[i] = new Label();
                labels[i].Text = namesHeaders[i];
                labels[i].Name = namesHeaders[i];
                labels[i].Location = new Point(20, 30 + i * 60);
                labels[i].Size = new System.Drawing.Size(60, 20);
                labels[i].Visible = true;
                this.Controls.Add(labels[i]);

                textBoxes[i] = new TextBox();
                textBoxes[i].Location = new Point(120, 30 + i * 60);
                textBoxes[i].Visible = true;
                this.Controls.Add(textBoxes[i]);
            }
            this.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            query += " VALUES( ";
            for (int i = 0; i < countElement; i++)
            {
                query += "\'" + textBoxes[i].Text + "\', ";
            }
            query = query.Remove(query.Length - 2);
            query += ")";
            MySqlConnection con = new MySqlConnection(Connection.getConnectionString());
            MySqlCommand command = new MySqlCommand(query, con);
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            con.Close();
        }

        private void очиститьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < countElement; i++)
            {
                textBoxes[i].Text = "";
            }
        }

        private void InsertForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
