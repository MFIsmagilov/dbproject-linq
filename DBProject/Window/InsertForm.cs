﻿using DBProject.Tables;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class InsertForm : Form
    {
        private Label[] labels;
        private TextBox[] textBoxes;
        private int countElement;
        string[] namesHeaders;
        private AdminContext _context;
        string nameTable;
        AdminRole formParent;
        public InsertForm(AdminContext _context, string[] namesHeaders, string nameTable)
        {
            InitializeComponent();
            this.nameTable = nameTable;
            this._context = _context;
            this.namesHeaders = namesHeaders;
            this.countElement = namesHeaders.Count();
            labels = new Label[countElement];
            textBoxes = new TextBox[countElement];
        }

        private void InsertForm_Load(object sender, EventArgs e)
        {
            formParent = this.Owner as AdminRole;

            int height = 0;
            //расположение элементов lable and textbox на форме
            for (int i = 0; i < countElement; i++)
            {
                height = 60 + i * 60;
                labels[i] = new Label();
                labels[i].Text = namesHeaders[i];
                labels[i].Name = namesHeaders[i];
                labels[i].Location = new Point(20, height);
                labels[i].Size = new System.Drawing.Size(60, 20);
                labels[i].Visible = true;
                this.Controls.Add(labels[i]);

                textBoxes[i] = new TextBox();
                textBoxes[i].Location = new Point(120, height);
                textBoxes[i].Visible = true;
                this.Controls.Add(textBoxes[i]);
            }

            // расположение кнопки ок и отмена
            this.button1.Location = new Point(button1.Location.X, height += 30);
            this.button2.Location = new Point(button2.Location.X, height);
            this.Size = new Size(this.Size.Width, height + 70);
            this.Refresh();
        }

        private void btnAdd(object sender, EventArgs e)
        {
            try
            {
                List<string> values = new List<string>();
                for (int i = 0; i < textBoxes.Length; i++)
                    values.Add(textBoxes[i].Text);
                _context.AddObject(this.nameTable, values);
                if (formParent != null)
                {
                    formParent.UpdateTable(this.nameTable);
                }
                this.Close();
            }catch(Exception ex) { MessageBox.Show(ex.ToString(), "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void очиститьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < countElement; i++)
            {
                textBoxes[i].Text = "";
            }
        }

        private void InsertForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
