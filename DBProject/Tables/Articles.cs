﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Статьи"
    /// </summary>
    [Table(Name = "Articles")]
    public class Articles:IEssenceDB
    {

        public Articles()
        {
            _admins = new EntityRef<Admins>();
        }
        EntityRef<Admins> _admins;
        public EntityRef<Admins> _idAdmins;
        [Association(Storage = "_idAdmins", ThisKey = "IdArticle", OtherKey = "IdAdmin", Name = "FK_admins_articles", IsForeignKey = true)]
        public Admins IdAdmins
        {
            get { return _admins.Entity; }
            set { _admins.Entity = value; }
        }

        private int _IdArticle;
        [Column(Name = "IdArticle", Storage="_IdArticle", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdArticle 
        {
            get { return _IdArticle; }
            set { _IdArticle = value; }
        }

        private DateTime _DateArticle;
        [Column(Name = "DateArticle", Storage = "_DateArticle", DbType = "DATE", CanBeNull = false)]
        public DateTime DateArticle
        {
            get { return _DateArticle; }
            set { _DateArticle = value; }
        }

        private int _IdAdmin;
        [Column(Name = "IdAdmin", Storage = "_IdAdmin", DbType = "INT(10)", CanBeNull = false)]
        public int IdAdmin
        {
            get { return _IdAdmin; }
            set { _IdAdmin = value; }
        }

        private string _Subject;
        [Column(Name = "Subject", Storage="_Subject", DbType = "VARCHAR(50)", CanBeNull = true)]
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("DateArticle");
            l.Add("IdArticle");
            l.Add("IdAdmin");
            l.Add("Subject");
            return l;
         
        }

        public void SetValueAtribute(List<string> values)
        {
            this.DateArticle = Convert.ToDateTime(values[0]);

            this.IdArticle = Convert.ToInt32(values[1]);
            this.IdAdmin = Convert.ToInt32(values[2]);
            this.Subject = values[3];
        }
    }
    public class TypeArticle : ITypeTables
    {
        public int IdArticle { get; set; }
        public DateTime DateArticle { get; set; }
        public int IdAdmin { get; set; }
        public string Subject { get; set; }

        public TypeArticle(Articles a)
        {
            this.IdArticle = a.IdArticle;
            this.DateArticle = a.DateArticle;
            this.IdAdmin = a.IdAdmin;
            this.Subject = a.Subject;
        }
        public TypeArticle(int idArticle, int idAdmin, string subject, DateTime dateArticle)
        {
            this.IdArticle = idArticle;
            this.DateArticle = dateArticle;
            this.IdAdmin = idAdmin;
            this.Subject = subject;
        }
        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("DateArticle");

            l.Add("IdArticle");
            l.Add("IdAdmin");
            l.Add("Subject");            
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.DateArticle.ToString());

            l.Add(this.IdArticle.ToString());
            l.Add(this.IdAdmin.ToString());
            l.Add(Subject != null? this.Subject.ToString() : null);
           
            return l.ToArray();
        }
        public override string ToString()
        {
            return "IdArticle  : " + IdArticle.ToString() + "\n" +
                "DateArticle: " + DateArticle.ToString() + "\n" +
                   "IdAdmin    : " + IdAdmin.ToString() + "\n" +
                   "Subject    : " + Subject + "\n";
                   
        }
    }
}
