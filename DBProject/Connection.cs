﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBProject
{
    public static class Connection
    {

        public const string ServerName = "localhost";
        public const string UserName = "root";
        public const string Password = "root";
        public const string DbName = "my_site1";
        public const string CharSet = "utf8";
        // public const string ConnectionString = String.Format("server={0};user={1}; password={2}; database={3}"
        //, Connection.ServerName, Connection.UserName, Connection.Password, Connection.DbName);
        /*
         public string connectionString { get; set; }
         public MySite dataBase { get; set; }
         public Connection(string connectionString = "server=localhost;" +
                                                     "user id=root; " +
                                                     "password=root; " +
                                                     "database=my_site1; " +
                                                     "charset=utf8")
         {
             this.connectionString = connectionString;
             this.dataBase = new MySite(new MySqlConnection(this.connectionString));
         }

         public void Open()
         {
             dataBase.Connection.Open();
         }

         public void Close()
         {
             dataBase.Connection.Close();
         }
     }*/
        public static string getConnectionString()
        {
            return String.Format("server={0};user={1}; password={2}; database={3}; CharSet={4}"
               , 
               Connection.ServerName, 
               Connection.UserName, 
               Connection.Password, 
               Connection.DbName,
               Connection.CharSet);
        }
}
}
