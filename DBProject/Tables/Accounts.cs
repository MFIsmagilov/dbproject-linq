﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using DbLinq.Data.Linq;
using nwind;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Аккаунты"
    /// </summary>
    [Table(Name = "Accounts")]
    public class Accounts
    {
        [Column(Name = "Email", DbType = "VARCHAR(50)", CanBeNull = false)]
        public string Email { get; set; }
        [Column(Name = "Phone", DbType = "VARCHAR(255)", CanBeNull = false)]
        public string Phone { get; set; }
        [Column(Name = "Password", DbType = "VARCHAR(255)", CanBeNull = false)]
        public string Password { get; set; }
        [Column(Name = "IdUsers", DbType = "INT(10)", IsPrimaryKey = true, CanBeNull = false)]
        public int IdUsers { get; set; }
    }

    public class TypeAccount: ITypeTables
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public int IdUsers { get; set; }

        public TypeAccount(int idUsers, string email, string phone, string password)
        {
            this.IdUsers = idUsers;
            this.Email = email;
            this.Phone = phone;
            this.Password = password;
        }

        public TypeAccount(Accounts a)
        {
            this.IdUsers = a.IdUsers;
            this.Email = a.Email;
            this.Phone = a.Phone;
            this.Password = a.Password;
        }
        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdUsers");
            l.Add("Email");
            l.Add("Phone");
            l.Add("Password");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdUsers.ToString());
            l.Add(this.Email);
            l.Add(this.Phone);
            l.Add(this.Password);
            return l.ToArray();
        }

        public override string ToString()
        {
            return "IdUsers : " + IdUsers.ToString() + "\n" +
                   "Email   : " + Email + "\n" +
                   "Phone   : " + Phone + "\n" +
                   "Password: " + Password + "\n";
        }
    }
}
