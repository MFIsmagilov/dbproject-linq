﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace DBProject.Tables
{
    /// <summary>
    /// Таблица "Читают"(М:М)
    /// </summary>
    [Table(Name = "Reads")]
    class Reads
    {
        [Column(Name = "Id", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
        public int Id { get; set; }
        [Column(Name = "IdArticle", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
        public int IdArticle { get; set; }
    }
    public class TypeReader
    {
        public int Id { get; set; }
        public int IdArticle { get; set; }

        public TypeReader(int id, int idArticle)
        {
            this.Id = id;
            this.IdArticle = idArticle;
        }

        public override string ToString()
        {
            return "Id        : " + Id.ToString() + "\n" +
                   "IdArticle : " + IdArticle.ToString() + "\n";
        }
    }
}
