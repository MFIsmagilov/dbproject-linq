﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace DBProject.Tables
{

    /// <summary>
    /// Таблица "Комментарии"
    /// </summary>
    [Table(Name = "Comments")]
    public class Comments: IEssenceDB
    {
        private DateTime _DateComment;
        [Column(Name = "DateComment", Storage="_DateComment", DbType = "Date", CanBeNull = false)]
        public DateTime DateComment 
        {
            get { return _DateComment; }
            set { _DateComment  = value; }
        }

        private int _IdArticle;
        [Column(Name = "IdArticle", Storage="_IdArticle", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
        public int IdArticle
        {
            get { return _IdArticle; }
            set { _IdArticle  = value; }
        }

        private int _Id;
        [Column(Name = "Id", Storage="_Id", DbType = "INT(10)", IsPrimaryKey = true,  CanBeNull = false)]
        public int Id 
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private int _NumberComment;
        [Column(Name = "NumberComment", Storage = "_NumberComment ", DbType = "Int(20)", IsPrimaryKey = true, CanBeNull = false)]
        public int NumberComment 
        {
            get { return _NumberComment; }
            set { _NumberComment  = value; }
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdArticle");
            l.Add("Id");
            l.Add("NumberComment");
            l.Add("DateComment");
            return l;
        }

        public void SetValueAtribute(List<string> values)
        {
            this.IdArticle = Convert.ToInt32(values[0]);
            this.Id = Convert.ToInt32(values[1]);
            this.NumberComment = Convert.ToInt32(values[2]);
            this.DateComment = Convert.ToDateTime(values[3]);
        }
    }
    public class TypeComment : ITypeTables
    {
        public DateTime DateComment { get; set; }
        public int IdArticle { get; set; }
        public int Id { get; set; }
        public int NumberComment { get; set; }

        public TypeComment(Comments c)
        {
            this.DateComment = c.DateComment;
            this.IdArticle = c.IdArticle;
            this.Id = c.Id;
            this.NumberComment = c.NumberComment;
        }
        public TypeComment(int idArticle, int id, int numberComment, DateTime dateComment)
        {
            this.IdArticle = idArticle;
            this.Id = id;
            this.NumberComment = numberComment;
            this.DateComment = dateComment;
        }

        public List<string> GetNameAtribute()
        {
            List<string> l = new List<string>();
            l.Add("IdArticle");
            l.Add("Id");
            l.Add("NumberComment");
            l.Add("DateComment");
            return l;
        }

        public object[] GetObject()
        {
            List<string> l = new List<string>();
            l.Add(this.IdArticle.ToString());
            l.Add(this.Id.ToString());
            l.Add(this.NumberComment.ToString());
            l.Add(this.DateComment.ToString());
            return l.ToArray();
        }

        public override string ToString()
        {
            return "IdArticle    : " + IdArticle.ToString() + "\n" +
                   "Id           : " + Id.ToString() + "\n" +
                   "NumberComment: " + NumberComment.ToString() + "\n" +
                   "DateComment  : " + DateComment.ToString() + "\n";
        }
    }
}
